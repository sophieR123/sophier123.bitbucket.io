#Sophie Rollo
*Sophiecevrollo@gmail.com 	https://www.linkedin.com/in/sophiecevrollo/  http://www.sophierollo.com/*
##Personal Statement
I am available from June 15th 2020 for full time positions. 
Currently studying Japanese Language in Tokyo whilst on break from university. I am a driven student looking for an opportunity which will help me progress my learning further, following completion of my degree at University of East Anglia. I’m looking for a challenge that will push my boundaries of knowledge and help me develop as a software engineer, a position which allows me to make an impact on the team and product I am working on and to improve both myself and my work simultaneously.  
##Experience
###Upcoming
###Neo4j – Intern
I will be spending a month with Neo4j throughout April as a developer. I will be working directly under Mark Needham and Michael Hunger. During this internship I expect to learn a lot more about Neo4j, its applications and I hope I will progress further as a developer.  
###Past
###The User Story – Intern
I spent three days shadowing the team at the user story to gain an insight into the User Experience development workflow. 
###Tech Marionette—Developer
Tech Marionette is a local start-up founded to address issues large corporates face with their IT infrastructure. They work closely with the UEA Computing department and are known for taking on the best and brightest students for internships and year in industry placements. During my year in industry I learned Go, Cypher, and Bash, and extended my knowledge of Python. I also learned about using JSON as a messaging format, graph databases (Neo4j), AWS EC2, and Docker. I was encouraged to attended training workshops, events and conferences to help progress me further as a developer and gained experience of working alongside highly regarded industry professionals. This role has given me experience of JIRA and further experience using Bitbucket.
###TIDI Home—Contracted Developer
After working alongside other TIDI developers in university, they recommended me for the contracted position at TIDI. My role includes using Typescript, Ionic and NodeJS to build an application for IOS and Android devices. I am working as a full stack developer, designing the back end including the structure of the data, which is stored on Google Firebase. We utilise Workast and Gitlab in this role. 

##Achievements
* Certified Neo4j Professional 95.5% (84 of 88) 		Download : https://bit.ly/2IGAwaL 
* TEDxUniversityofEastAnglia 2018  Co-organiser 	https://www.ted.com/tedx/events/24901
* Winner SyncTheCity 2018 Developer for Team Sorti	https://syncthecity.com
* Code First: Girls UEA 2018 Lead Instructor 		https://www.codefirstgirls.org.uk

##Education
###University of East Anglia 
Computing Sciences with a foundation year and year in industry
On track for a first, I have completed a foundation year and 3 undergraduate years at UEA. I am due to return for my final year of study September 23rd September 2019.
####Year 2 Notable Marks
* Weighted mean mark—72.56% 
* Software Engineering 1—Overall Mark 83.90%
* Data Structures and Algorithms—Module Assessment Exercises 2—78.69%
* Graphics 1—Examination 83.33%
* Architectures and Operating Systems—Architecture Assignment—80%
* Programming 2—Java programming 88%
* Analogue and Digital Electronics—Traffic Light Controller—92%
####Year 1 Notable Marks
* Computing Principles—Electronic Tests—84.67%
* Mathematics for Computing A—Coursework Quizzes—72.40%
* Programming 1—Module Assessment Exercises—88%
* Web-Based Programming—Website Development—91% 
* Systems Development—Overall Mark—71.50%
 
##Volunteering Experience
### Step into Tech Techathon Tech Mentor
 Worked with 80 8-18-year olds over 2 days to help them solve challenges posed by Norfolk County Council, Norwich University of the Arts and Microsoft. This was an exciting opportunity to help the future generation of programmers and tech-oriented individuals get an insight into what it was like to be a programmer with a task to solve.
### Code First Girls Lead Mentor
 Working alongside other instructors to plan lessons, write scripts and practice demonstrations to teach a group of 20 girls how to build a website using HTML5, CSS, JavaScript and Bootstrap.
*References available on request*
